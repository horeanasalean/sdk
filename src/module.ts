import { NgModule } from '@angular/core';
import { MessageService } from './services/sample.service';

@NgModule({
  providers: [
    MessageService
  ]
})
export class SDKModule { }
